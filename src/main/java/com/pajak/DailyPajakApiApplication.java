package com.pajak;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DailyPajakApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DailyPajakApiApplication.class, args);
	}

}

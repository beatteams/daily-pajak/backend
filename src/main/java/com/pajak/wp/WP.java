package com.pajak.wp;

import com.pajak.audit.Auditable;
import com.pajak.user.User;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Entity
@Table(name = "tbl_wp")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class WP extends Auditable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String npwpd;
    private String nama;

    @Column(length = 1000)
    private String alamat;
    private Date tanggal_daftar;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;

    private Boolean deleted;
}

package com.pajak.wp;

import org.springframework.data.jpa.repository.JpaRepository;

public interface WPRepository extends JpaRepository<WP, Long> {
}

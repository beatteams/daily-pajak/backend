package com.pajak.wp;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
public class WPService {

    @Autowired
    private WPRepository wpRepository;

    public WP create(WP wp){
        return wpRepository.save(wp);
    }

    public WP findOne(Long id){
        return wpRepository.findById(id).get();
    }

    public List<WP> findAll(){
        return wpRepository.findAll();
    }

    public void removeOne(Long id){
        wpRepository.deleteById(id);
    }

}

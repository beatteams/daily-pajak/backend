package com.pajak.wp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/wp")
public class WPController {

    @Autowired
    private WPService wpService;

    @GetMapping
    public Iterable<WP> findAll(){
        return wpService.findAll();
    }

    @PostMapping
    public WP create(@RequestBody WP wp){
        return wpService.create(wp);
    }

}
